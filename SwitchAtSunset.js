// Switch relay ON at local Sunset only if Input status is OFF

// CONFIG START
let CONFIG = {
    sunsetOffset: 20 *60*1000,    // delay upon sunset
    sunURL: 'https://www.mysite.com/date_sun.php',
    latitude: 41.9027,            // default latitude
    longitude: 12.4963,           // default longitude
    timezone: "Europe/Rome",      // default timezone
    input: 0,                     // Shelly input channel ID
    rele: 0,                      // Shelly output channel ID
}
// CONFIG END

let SETUP = {
    sunset: 0,
    timeToSunset: 0,
    timerSunset: 0,
    timerCheck: 0,
}


function switchOut() {
    Shelly.call("Input.GetStatus",
        { id: CONFIG.input},
        function (resp) {
            print(resp);
            if (! resp.state) {
                Shelly.call("Switch.set", {'id': CONFIG.rele, 'on': true});
                print("DEBUG: turn ON relay.")
            } else {
                print("DEBUG: relay already ON!")
            }
        },
        null
    )
}

function manageSunset(res, error_code, error_msg) {
    if (res.code === 200) {
        let st = JSON.parse(res.body);
        print(st);
        if (st.sunset > 0) {
            SETUP.sunset = parseInt(st.sunset) *1000;
            print("Sunset is at: ", SETUP.sunset, Date(SETUP.sunset));
            SETUP.timeToSunset = SETUP.sunset - Date.now() + CONFIG.sunsetOffset;
            if (SETUP.timeToSunset > 0) {
                if (SETUP.timerSunset > 0) {
                    Timer.clear(SETUP.timerSunset)
                }
                SETUP.timerSunset = Timer.set(SETUP.timeToSunset, false, switchOut);
                print("Sunset in: ", SETUP.timeToSunset, " milliseconds. Timer n. : ", SETUP.timerSunset, " set at ", Date(Date.now() + SETUP.timeToSunset));
            } else {
                print("Today sunset passed yet!");
            }
        } else {
            print("DEBUG: Sunset time error");
        }
    } else {
        print("DEBUG: Sun query error");
    }
}

function getSunset() {
    url_ = CONFIG.sunURL +'?lat='+ CONFIG.latitude +'&lon='+ CONFIG.longitude +'&timezone='+ CONFIG.timezone;
    print("DEBUG: ", url_);
    Shelly.call(
        "http.get", {
            url: url_
        }, manageSunset);
}


// Periodic sunset check
function checkSun() {
    return Timer.set(6*60*60*1000, true, getSunset);
}


// Initialize script and confuguration
function Init() {
    // Retrive location and time zone from device configuration and initialize config
    Shelly.call("Shelly.GetConfig",
        {id:null},
        function (resp) {
            print("DEBUG: device location: ", resp.sys.location);
            CONFIG.latitude = resp.sys.location.lat;
            CONFIG.longitude = resp.sys.location.lon;
            CONFIG.timezone = resp.sys.location.tz;
            // Device startup sunset check
            getSunset();
        }
    );

    
    // Periodica sunset check
    SETUP.timerCheck = checkSun();
}


// Initialize config and setup
Init();

print('Device started, local time is: ', Date(Date.now()));
