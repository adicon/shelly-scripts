<?php 
        $lat = ($_GET['lat'] ? $_GET['lat'] : 0.0000); 
        $lon = ($_GET['lon'] ? $_GET['lon'] : 0.0000); 
        $timezone = ($_GET['timezone'] ? $_GET['timezone'] : 'Etc/GMT'); 
 
        date_default_timezone_set($timezone); 
        $sun_info = date_sun_info(time(), $lat, $lon); 
        $sun_info['lat'] = floatval($lat); 
        $sun_info['lon'] = floatval($lon); 
        $sun_info['timezone'] = $timezone; 
 
        header("Content-Type: application/json"); 
        // echo $lat, $lon; 
        echo json_encode($sun_info, JSON_FORCE_OBJECT); 
        exit(); 
?>
